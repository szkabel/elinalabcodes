function plotVectorImage(nucImg, cellImg, spotList, distMtx, outName)

close all;

figure(1);
figure(2);

sx = 0;

for i=1:max(nucImg(:))
   
    nucBW = nucImg == i;
    cellBW = cellImg == i;

%     [currentSpotsX currentSpotsY] = find(uint16(cellBW) .* spotImg > 0);
    counter = 1;
    currentSpotsX = [];
    currentSpotsY = [];
    currentSpotsRH = [];
    for j=1:size(spotList, 1)
        if cellImg(ceil(spotList(j, 2)+1), ceil(spotList(j, 1))+1) == i
            currentSpotsX(counter) = spotList(j, 2);
            currentSpotsY(counter) = spotList(j, 1);
            currentSpotsRH(counter) = spotList(j, 3);    
            counter = counter + 1;
        end        
    end
    
    nucContour = bwboundaries(nucBW);
    cellContour = bwboundaries(cellBW);
    
    figure(1);
    fill(cellContour{1}(1:5:end,2), cellContour{1}(1:5:end,1), [0.5 0 0]);
    hold on;
    fill(nucContour{1}(1:5:end,2), nucContour{1}(1:5:end,1), [0 0 .5]);
    hold on;     
    for j=1:length(currentSpotsY)
        
        if currentSpotsRH(j) > 0        
            rectangle('Position', [currentSpotsY(j)-currentSpotsRH(j) currentSpotsX(j)-currentSpotsRH(j) currentSpotsRH(j) * 2 currentSpotsRH(j) * 2], 'Curvature', [1 1], 'EdgeColor',[0 .5  0], 'FaceColor', [0 1 0], 'LineWidth', 0.1);
        end
        
    end
    
    [rList, cList] = find(distMtx > 0);
    for k=1:numel(rList)
        if (cellImg(round(spotList(rList(k), 2)), round(spotList(rList(k), 1))) == i) && ...
                (cellImg(round(spotList(cList(k), 2)), round(spotList(cList(k), 1))) == i)
            line([spotList(rList(k), 1)  spotList(cList(k), 1)], [spotList(rList(k), 2)  spotList(cList(k), 2)], 'Color',[1 1  1], 'LineWidth', 0.3);            
        end
    end                 
    
    
    close(2)
    figure(2);        
    dx = 0;
    dy = 0;
%     [cx cy] = find(cellBW == 1);
%     dx = min(cx); dy = min(cy);
%     sx = sx + max(cx) - min(cx) + 20
%     dx = double(dx + sx);
    fill(cellContour{1}(1:5:end,2)-dy, cellContour{1}(1:5:end,1)-dx, [0.5 0 0], 'LineWidth', 0.1);
    hold on;
    fill(nucContour{1}(1:5:end,2)-dy, nucContour{1}(1:5:end,1)-dx, [0 0 .5], 'LineWidth', 0.1);
    hold on;     
    for j=1:length(currentSpotsY)
        if currentSpotsRH(j) > 0                
            rectangle('Position', [currentSpotsY(j)-currentSpotsRH(j)-dy currentSpotsX(j)-currentSpotsRH(j)-dx currentSpotsRH(j) * 2 currentSpotsRH(j) * 2], 'Curvature', [1 1], 'EdgeColor',[0 .5 0], 'FaceColor', [0 1 0], 'LineWidth', 0.1);
        end
    end

    [rList, cList] = find(distMtx > 0);
    for k=1:numel(rList)
        if (cellImg(uint16(spotList(rList(k), 2)), uint16(spotList(rList(k), 1))) == i) && ...
                (cellImg(uint16(spotList(cList(k), 2)), uint16(spotList(cList(k), 1))) == i)
            line([spotList(rList(k), 1)  spotList(cList(k), 1)], [spotList(rList(k), 2)  spotList(cList(k), 2)], 'Color',[1 1  1], 'LineWidth', 0.3);            
        end
    end                 
    
    
    figure(2);
    axis equal;    
    
    print('-f2', '-deps', [outName '_' sprintf('%0.3d', i) '.eps']);
    print('-f2', '-dpdf', [outName '_' sprintf('%0.3d', i) '.pdf']);
    
end

figure(1);
axis equal;


print('-f1', '-deps', [outName '.eps']);
print('-f1', '-dpdf', [outName '.pdf']);
