folderName = 'F:\nikon2 after 31oct2016\E32-4 31Oct2016 LD auto2\2016E32decon';

fileList = dir([folderName '\*.tif']);

mkdir( [folderName '\out\']);

mkdir( [folderName '\out\anal1']);
mkdir( [folderName '\out\anal2']);
mkdir( [folderName '\out\anal3']);
mkdir( [folderName '\out\anal4']);
mkdir( [folderName '\out\anal5']);


for i=1:length(fileList)
   
    img = [];
    
    fileName = [folderName '\' fileList(i).name];
    
    fileOutName = [folderName '\out\' fileList(i).name];
    
    slices = length(imfinfo(fileName));
    
    for j=1:slices

        img(:,:,j) = imread(fileName, j);
        
    end
    
    imgOut = max(img, [], 3);
    
    imwrite(uint16(imgOut), fileOutName);
        
    disp([fileOutName '... Done']);
    
end
