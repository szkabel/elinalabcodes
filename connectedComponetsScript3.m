clear all;

analFolder = 'F:\20161019_102549_934 A431 CRISPR KO CM\2016 10 19 A431 decon\out\';
%analFolder = 'd:\�bel\SZBK\Projects\SimonLipids\Hospital\Errors'
%analFolder = 'd:\�bel\SZBK\Projects\SimonLipids\LargerDroplets\Deconvolved';
analFileName = 'DefaultOUT__9.mat';
%analFileName = 'DefaultOUT__8.mat';

outputFolder = analFolder;

%{
%User friendly version
analFolder = uigetdir(pwd,'Specify your analysis folder! (anal folders inside)');
analFolder = [analFolder filesep];
analFile = uigetfile(analFolder,'Select your CellProfiler output file!');
outputFolder = uigetdir(analFolder,'Specify your output folder!');
%}

maxDist = 2;

load(fullfile(analFolder,analFileName));

f = zeros(1000, 5000);

% start global statistics
fCellBased = fopen([outputFolder filesep 'CellBased.csv'], 'w');
fClusterBased = fopen([outputFolder filesep 'ClusterBased.csv'], 'w');
fSingleSpot = fopen([outputFolder filesep 'SingleSpot.csv'], 'w');

% write global stat headers
fprintf(fClusterBased, 'fileName, CellID, ClusterID, ClusterSize, DropletSizeMean(pixel), DropletSizeMedian(pixel), DropletSizeStd(pixel), DropletSizeMean(Um2), DropletSizeMedian(Um2), DropletSizeStd(Um2), DropletIntensityMean, DropletIntensityMedian, DropletIntensityStd\n');
fprintf(fSingleSpot, 'fileName, CellID, ClusterID, ClusterSize, DropletSize(pixel), DropletSize(Um2), DropletIntensity\n');
fprintf(fCellBased, 'fileName, CellID, dropletNumber, MeanDropletSize(pixel), MeanDropletSize(Um2), MeanDropletIntensity, StdDropletSize(pixel), StdDropletSize(Um2), StdDropletIntensity\n');


for i=1:length(handles.Measurements.Image.FileNames)   
    % open current image
    greenImage = imread(fullfile(analFolder,handles.Measurements.Image.FileNames{i}{2}));
    % open labeled images
    [~, imageName, ext]= fileparts(handles.Measurements.Image.FileNames{i}{1});
    smallDroplets = imread(fullfile(analFolder,'anal4',[imageName '_SmallDroplets' ext]));
    largeDroplets = imread(fullfile(analFolder,'anal4',[imageName '_LargeDroplets' ext]));
    NucleiImage = imread(fullfile(analFolder,'anal4',[imageName '_Nuclei' ext]));
    CellImage = imread(fullfile(analFolder,'anal4',[imageName '_Cells' ext]));
    
    disp(['--Image ' num2str(i) ' loaded...']);
    
    % remove overlaps
    overlapping = smallDroplets .* uint16(largeDroplets > 0);
    deletedSpots = unique(overlapping(:)); deletedSpots = deletedSpots(2:end);
    keptSpots = setdiff(1:max(smallDroplets(:)), deletedSpots);
    
    % load dots, area, intensity
    dropletLocation = handles.Measurements.Spots.Location{i};
    largeDropletLocation = handles.Measurements.LargeSpots.Location{i};
    dropletArea = handles.Measurements.Endosomes.AreaShape{i}(:,1);
    largeDropletArea = handles.Measurements.LargeEndosomes.AreaShape{i}(:,1);
    dropletIntensity = handles.Measurements.Endosomes.Intensity_OrigGreen{i}(:,2);
    largeDropletIntensity = handles.Measurements.LargeEndosomes.Intensity_OrigGreen{i}(:,2);
    
    dropletLocationAll = [dropletLocation(keptSpots, :); largeDropletLocation];
    dropletRadiiAll = sqrt([dropletArea(keptSpots); largeDropletArea] ./ pi) ;
    dropletIntensityAll = [dropletIntensity(keptSpots); largeDropletIntensity];

    % if no large droplets delete last line
    if dropletLocationAll(end, 2)  == 0
        dropletLocationAll(end, :) = [];
        dropletRadiiAll(end) = [];
        dropletIntensityAll(end) = [];        
    end
        
    disp(['----Image ' num2str(i) ' contains ' num2str(size(dropletLocationAll, 1)) ' droplets...']);
    
    % build distance matrix
    distMtx = squareform(pdist(dropletLocationAll));
    % determine min spanning tree and remove long edges
    if ~isempty(distMtx)
        minGraph  = graphminspantree(sparse(distMtx));
    else
        minGraph = sparse([]);
    end
    toRem = find(minGraph == 0);
    distMtx(toRem) = 0;
    % remove points far from eachother
    torem = find(distMtx > maxDist);
        
    % replace with radii distances
    %%%%
    if ~isempty(distMtx)
        for ii=1:length(dropletRadiiAll)
            for jj=1:length(dropletRadiiAll)
                if distMtx(ii, jj) > dropletRadiiAll(ii) + dropletRadiiAll(jj) + maxDist
                    distMtx(ii, jj) = 0;
                end
            end
        end
    end
    
    %     distMtx(torem) = 0;
    [S, C] = graphconncomp(sparse(distMtx), 'Weak', 'true');    
    
    dd=[dropletLocationAll' ; dropletRadiiAll'; dropletIntensityAll']';
    
    if size(dd,1) == 1
        S = 1;
        C = 1;
    end
    
    disp(['----Image ' num2str(i) ' contains ' num2str(S) ' connected components...']);
        
        
    % draw cells
     plotVectorImage(NucleiImage, CellImage, dd, distMtx, [outputFolder filesep imageName]);
    
    % create statistics    
    %with or without red intensities.
    %singleCellStatistics3(NucleiImage, CellImage, dd, distMtx, imageName, fCellBased, fSingleSpot, fClusterBased, outputFolder, redIntensities);
    singleCellStatistics3(NucleiImage, CellImage, dd, distMtx, imageName, fCellBased, fSingleSpot, fClusterBased, outputFolder);
    
        
    %f is an 'image' but rather a table. Each row corresponds to 1 image
    %and the column corresponds to a given sized cluster. The entry there
    %tells you the number of that sized clusters within that image.
    for j=1:S
        f(i, sum(C == j)) = f(i, sum(C==j)) + 1; 
    end;
    
    
end


fclose(fCellBased); 
fclose(fSingleSpot);
fclose(fClusterBased);
