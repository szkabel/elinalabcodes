function plotConvHull(points)
% Points should be an n by 2 matrix.
    idx = convhull(points(:,1),points(:,2));
    plot(points(idx,1),points(idx,2));
end

