function plotConvHullOnImage( imgName, points, clusterIndices )
%plots convex hull of identified cluster on the top of an image
%   cluesterIndices must be exactly as long as many rows there are in
%   points.

img = imread(imgName);
imshow(img);
hold on;
for i = 1:max(clusterIndices)
    if ~isempty(clusterIndices(clusterIndices == i))
        plotConvHull(points(clusterIndices == i,:))
    end
end

hold off;


end

