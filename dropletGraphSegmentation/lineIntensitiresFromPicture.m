function [ intensities ] = lineIntensitiresFromPicture( image, p1,p2)
% AUTHOR:   Abel Szkalisity
% DATE:     September 28, 2016
% NAME:     drawLineToPicture
%
% Given two points it gets the pixel intensitities of a line from point p1 to point p2.
% The points' coordinates are in the coordinate system of
% the picture matrix. If any of the points is out of the image coordinates
% then the original picture is returned.
%
% INPUT:
%   image           An image to get the line from
%   p1,p2           Two element vectors the end points of the line section
%
% OUTPUT:
%   intensities     The image with the line.

i1 = size(image,1);
i2 = size(image,2);

flip = 0;

%if the point is within the image
if p1(1)>=0 && p1(2)>=0 && p2(1)>=0 && p2(2)>=0 && p1(1)<=i1  && p2(1)<=i1 && p1(2)<=i2  && p2(2)<=i2
    %swap the points to have p1 at the front
    if p1(1)>=p2(1)
        buff = p1;
        p1 = p2;
        p2 = buff;
    end
        
    if (p2(1)-p1(1)~=0)
    %if the line is not vertical
        slope = (p2(2)-p1(2))/(p2(1)-p1(1));
        if slope>1 || slope<-1         
            i2 = i1;            
            buff = p1(2);
            p1(2) = p1(1);
            p1(1) = buff;
            buff = p2(2);
            p2(2) = p2(1);
            p2(1) = buff;
            slope = 1./slope;
            flip = 1;
        end
        %x and y are in matrix meaning
        x = p1(1):p2(1);
        y = round( (x-p1(1))*slope + p1(2));
        y(y==0) = 1;
        y(y==i2+1) = i2; 
        if flip
            buff = x;
            x = y;
            y = buff;
        end;
        intensities = zeros(1,length(x));
        for i=1:length(x)            
             intensities(i) = image(x(i),y(i),:);
        end        
    else    
    %if its vertical (because of the rotation it seems horizontal on the picture)
    
        %the p1 is lower
        if p1(2)>=p2(2)
            buff = p1;
            p1 = p2;
            p2 = buff;
        end
        intensities = zeros(1,p2(2)-p1(2)+1);
        for i=p1(2):p2(2)
            intensities(i) = image(p1(1),i,:);
        end        
    
    end    
end

end

