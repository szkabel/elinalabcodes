%testScript to run on lipid images
%needs a handles from CellProfiler (only for the image names)

%as the weight down by pixel intensities is a very time-consuming process
%we introduce a threshold for the maximum distance that we take into
%account for the weight down.

maxDistanceThreshold = 40;

inputFolderToDraw = 'D:\�bel\SZBK\Projects\SimonLipids\LargerDroplets\Deconvolved\anal1\';
inputFolderWithBW = 'D:\�bel\SZBK\Projects\SimonLipids\LargerDroplets\Deconvolved\anal4\';
originalLipidFolder = 'D:\�bel\SZBK\Projects\SimonLipids\LargerDroplets\Deconvolved\';
lipidImageNameNumber = 2;
otherImageNameBaseNumber = 1; %from your different channel names which one was the base to your created images
postFixForNuclei = '_Nuclei';
extForBW = '.tif';
extForMerged = '.png';
nofContourPoints = 8;

%for all image
for i = 1:length(handles.Measurements.Image.FileNames)
    smallDotsLocation = handles.Measurements.Spots.Location{i};
    largeDotsLocation = handles.Measurements.LargeSpots.Location{i};
    mergedSpots = [smallDotsLocation;largeDotsLocation];
    nofAllSpots = size(mergedSpots,1);
    centers = round(handles.Measurements.Nuclei.Location{i});
    lipidImg = imread(fullfile(originalLipidFolder,handles.Measurements.Image.FileNames{i}{lipidImageNameNumber}));
    
    [~,onlyName,~] = fileparts(handles.Measurements.Image.FileNames{i}{otherImageNameBaseNumber});
    grayImage = imread(fullfile(inputFolderWithBW,[onlyName postFixForNuclei extForBW]));
    
    %reservePlace for the full coordinate matrix
    fullCoordinates = zeros(nofAllSpots+(nofContourPoints+1),2);
    %fill in the first region
    fullCoordinates(1:nofAllSpots,:) = mergedSpots;
    
    nofCellsInThisImage = max(grayImage(:));
    
    %for each image
    for j = 1:nofCellsInThisImage
        %extend the fullCoordinates with the perimeter points
        bwImage = (grayImage == j);
        contour = bwboundaries(bwImage);
        contour = contour{1};
        indices = floor((1:nofContourPoints)*length(contour)./nofContourPoints);
        fullCoordinates(nofAllSpots+(j-1)*(nofContourPoints+1)+1:nofAllSpots+j*(nofContourPoints+1)-1,:) = contour(indices,[2 1]);
        fullCoordinates(nofAllSpots+j*(nofContourPoints+1),:) = centers(j,:);
    end
    
    fullDistanceMatrix = squareform(pdist(fullCoordinates));
    %zero out all elements which are greater than
    fullDistanceMatrix(fullDistanceMatrix > maxDistanceThreshold) = 0;
    %the matrix is symmetric and all the diagonal elements are zeros
    h = waitbar(0,sprintf('Calculate graph weights image %d/%d',i,length(handles.Measurements.Image.FileNames)));
    for j=1:size(fullDistanceMatrix,1)-1
        for k=(j:size(fullDistanceMatrix,2))            
            %??? how weight? For sure it must be inversely proportional.
            %calculate only for close enough points.
            if fullDistanceMatrix(j,k) ~= 0
                %connectionProfile = improfile(lipidImg,[fullCoordinates(j,1) fullCoordinates(k,1)],[fullCoordinates(j,2) fullCoordinates(k,2)]);
                %maskImage = drawLineToPicture(zeros(size(lipidImg)),[fullCoordinates(j,2) fullCoordinates(j,1)],[fullCoordinates(k,2) fullCoordinates(k,1)],'w');
                %masked = uint16(maskImage).*lipidImg;
                %indices = find(masked);
                %connectionProfile = masked(indices);                        
                connectionProfile = lineIntensitiresFromPicture(lipidImg,[fullCoordinates(j,2) fullCoordinates(j,1)],[fullCoordinates(k,2) fullCoordinates(k,1)]);
                if mean(connectionProfile) == 0
                    newDistance = fullDistanceMatrix(j,k);
                else
                    newDistance = fullDistanceMatrix(j,k)./(mean(connectionProfile));
                end
                fullDistanceMatrix(j,k) = newDistance;
                fullDistanceMatrix(k,j) = newDistance;                  
            end
        end
        waitbar(((j-1)*size(fullDistanceMatrix,1)+k)/size(fullDistanceMatrix,1).^2,h,sprintf('Calculate graph weights image %d/%d',i,length(handles.Measurements.Image.FileNames)));        
    end
    close(h);    
    
    cellCenterIndices = zeros(1,nofCellsInThisImage);
    %decrease distances within cellnuclei
    for j = 1:nofCellsInThisImage
        fullDistanceMatrix(nofAllSpots+(j-1)*(nofContourPoints+1)+1:nofAllSpots+j*(nofContourPoints+1)-1,nofAllSpots+j*(nofContourPoints+1)) = eps;
        fullDistanceMatrix(nofAllSpots+j*(nofContourPoints+1),nofAllSpots+(j-1)*(nofContourPoints+1)+1:nofAllSpots+j*(nofContourPoints+1)-1) = eps;
        cellCenterIndices(j) = nofAllSpots+j*(nofContourPoints+1);
    end
    
    %minGraph  = graphminspantree(sparse(fullDistanceMatrix));
    %cancel out edges not represented in the tree
    %fullDistanceMatrix(minGraph==0) = 0;
    %but reintroduce symmetry
    %fullDistanceMatrix = fullDistanceMatrix + fullDistanceMatrix';
    
    [~,onlyName,~] = fileparts(handles.Measurements.Image.FileNames{i}{otherImageNameBaseNumber});
    mergedImg = imread(fullfile(inputFolderToDraw,[onlyName extForMerged]));
    
    hh = figure();
    imshow(mergedImg);
    hold on;
    
    [rList, cList] = find(fullDistanceMatrix > 0);
    for j=1:length(rList)        
        line([fullCoordinates(rList(j),1),fullCoordinates(cList(j),1)],[fullCoordinates(rList(j),2),fullCoordinates(cList(j),2)],'Color',[1 1  1], 'LineWidth', 0.3);
    end
    print(hh,fullfile(originalLipidFolder,[onlyName '_spider']),'-dpng');
    hold off;
        
    identifiedCenter = zeros(nofCellsInThisImage,nofAllSpots);    
    for j = 1:length(cellCenterIndices)
        smallestDistancesFromCenter = DijsktraMutliple(fullDistanceMatrix,cellCenterIndices(j),[]);        
        identifiedCenter(j,:) = smallestDistancesFromCenter(1:nofAllSpots);
    end
    [values,identifiedCenter] = min(identifiedCenter);        
    
    hhh = figure();
    plotConvHullOnImage( fullfile(inputFolderToDraw,[onlyName extForMerged]), mergedSpots(values<inf,:) , identifiedCenter(values<inf))
    print(hhh,fullfile(originalLipidFolder,[onlyName '_segmented']),'-dpng');        
    
    disp('img ready');
    
end
