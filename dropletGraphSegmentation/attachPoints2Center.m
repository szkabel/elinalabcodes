function [ indices ] = attachPoints2Center( centerPoints, points2Relate )
% attachPoints2Center function to relate points in the Euclidean space into
% center points. It can work on arbitrary d dimensional data. n is the
% numbe of center points N is the number of points to relate, d is the
% dimension of the space
%   INPUTS:
%      centerPoints     an n by d matrix each row identifies a center point
%      points2Relate    an N by d matrix each row identifies a point to
%                       relate
%   OUTPUT:
%       indices         a vector with length N, indices(i) gives you the id
%                       of the related center point to ith point.

%for all points

indices = zeros(size(points2Relate,1),1);

for i=1:size(points2Relate,1)
    distFromCenters = sqrt(sum((centerPoints-repmat(points2Relate(i,:),size(centerPoints,1),1)).^2,2));
    [~,indices(i)] = min(distFromCenters);
end

end

