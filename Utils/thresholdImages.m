%script to treshold the nuclei (prepareNucleiWithKmeans)
%3-means clustering performed on the images and then you can specify which
%cluster to remain. (according to their position in a sorted order 1: lowest intensity --> k: highest)

imgExt = 'tif';
%inputFolder = 'D:\�bel\SZBK\Projects\Veijo\LipidVariation\Workspace\';
%inputFolder = 'D:\�bel\SZBK\Projects\Veijo\LipidVariation\EXP 213 OA 20 hr first exp\';
inputFolder = 'D:\�bel\SZBK\Projects\SimonLipids\TransfectedCells\Images\';
regEx = '*ch01.';
regEx2 = '*ch00.';
newChannel = 'ch03';

imgList = dir([inputFolder regEx imgExt]);
imgListBlue = dir([inputFolder regEx2 imgExt]);

se = strel('disk',25);
for i = 1:length(imgList)
    img = imread([inputFolder imgList(i).name]);
    imgBlue = imread([inputFolder imgListBlue(i).name]);    
    h = histogram(img);
    bw = h.BinWidth;
    v = h.Values;
    v2 = filter(ones(1,10).*(1/10),1,v);
    for j=2:length(v2)-1
        if ~(v2(j-1)<v2(j) || v2(j+1)<v2(j))
            break;
        end
    end
    j = max(1,j-10);
    level = j*bw;
    level = level/2^16;
    imgBW = im2bw(img,level);    
    imgBW = imclose(imgBW,se);        
    imwrite(uint16(imgBW).*imgBlue, [inputFolder imgList(i).name(1:end-8) newChannel '.' imgExt]);   
end