%script to treshold the nuclei (prepareNucleiWithKmeans)
%3-means clustering performed on the images and then you can specify which
%cluster to remain. (according to their position in a sorted order 1: lowest intensity --> k: highest)

k = 2;
imgExt = 'tif';
%inputFolder = 'D:\�bel\SZBK\Projects\Veijo\LipidVariation\Workspace\';
%inputFolder = 'D:\�bel\SZBK\Projects\Veijo\LipidVariation\EXP 213 OA 20 hr first exp\';
inputFolder =   'D:\�bel\SZBK\Projects\SimonLipids\TransfectedCells\Images\';
regEx = '*ch01*';
newChannel = 'ch03';
selectedCluster = 2;

imgList = dir([inputFolder regEx imgExt]);

for i = 1:length(imgList)
    img = imread([inputFolder imgList(i).name]);
    %img = imgaussfilt(img,13); %for the cyto
    img = imgaussfilt(img,25); %for the nuclei
    idx = kmeans(double(img(:)),k);
    representatives = zeros(1,k);
    for j = 1:k
        representatives(j) = mean(img(idx==j));
    end
    [~,sortedIndices] = sort(representatives);    
    clusterId = sortedIndices(selectedCluster);
    newImg = zeros(size(img));
    newImg(idx == clusterId) = img(idx == clusterId);
    
    %se = offsetstrel('ball',23,23);
    %{
    dilatedI = imdilate(newImg,se);
    seB = offsetstrel('ball',11,1);
    erodedImg = imerode(dilatedI,seB);    
    erodedImg2 = imerode(dilatedI,se);
    resImg = imdilate(erodedImg2,seB);
    %
    openedImg = imopen(newImg,se);
    resImg = imclose(openedImg,se);
    %}
    resImg = newImg;
    %}
    imwrite(uint16(resImg), [inputFolder imgList(i).name(1:end-8) newChannel '.' imgExt]);
end