 function singleCellStatistics3(nucImg, cellImg, spotList, distMtx, outName, fCellBased, fSingleSpot, fClusterBased, outputFolder, redIntensities)
%Partial doc of the parameters
% spotList: an N by 4 matrix
%   each row represents a droplet 1-2 column are coordinates (CP coordinate system)
%           3 is the radius 4 is the mean intensity.
%different usage if you use 9 or 10 input arguments. The extention is
%written to handle red intensities.
 
fOut = fopen([outputFolder filesep  outName '.csv'], 'w');

fOutSpot = fopen([outputFolder filesep  outName '_SingleSpot.csv'], 'w');

fOutCell = fopen([outputFolder filesep outName '_CellBased.csv'], 'w');

% header
if nargin<10
    fprintf(fOut, 'fileName, CellID, ClusterID, ClusterSize, DropletSizeMean(pixel), DropletSizeMedian(pixel), DropletSizeStd(pixel), DropletSizeMean(Um2), DropletSizeMedian(Um2), DropletSizeStd(Um2), DropletIntensityMean, DropletIntensityMedian, DropletIntensityStd\n');
    fprintf(fOutSpot, 'fileName, CellID, ClusterID, ClusterSize, DropletSize(pixel), DropletSize(Um2), DropletIntensity\n');
    fprintf(fOutCell, 'fileName, CellID, dropletNumber, MeanDropletSize(pixel), MeanDropletSize(Um2), MeanDropletIntensity, StdDropletSize(pixel), StdDropletSize(Um2), StdDropletIntensity\n');
else
    fprintf(fOut, 'fileName, CellID, ClusterID, ClusterSize, DropletSizeMean(pixel), DropletSizeMedian(pixel), DropletSizeStd(pixel), DropletSizeMean(Um2), DropletSizeMedian(Um2), DropletSizeStd(Um2), DropletIntensityMean, DropletIntensityMedian, DropletIntensityStd, CellIntensityRedIntegrated, CellIntensityRedMean\n');
    fprintf(fOutSpot, 'fileName, CellID, ClusterID, ClusterSize, DropletSize(pixel), DropletSize(Um2), DropletIntensity, CellIntensityRedIntegrated, CellIntensityRedMean\n');
    fprintf(fOutCell, 'fileName, CellID, dropletNumber, MeanDropletSize(pixel), MeanDropletSize(Um2), MeanDropletIntensity, StdDropletSize(pixel), StdDropletSize(Um2), StdDropletIntensity, CellIntensityRedIntegrated, CellIntensityRedMean\n');
end

scaleUm = 0.13333;
scaleUm2 = scaleUm*scaleUm;

spotList = double(spotList);

for i=1:max(nucImg(:))
   
    nucBW = nucImg == i;
    cellBW = cellImg == i;

    % find spots on the current cell   
    counter = 1;
    currentSpotsX = [];
    currentSpotsY = [];
    currentSpotsInt = [];
    currentSpotsRH = [];
    toKeep = [];
    
    for j=1:size(spotList, 1)
        if cellImg(ceil(spotList(j, 2)+1), ceil(spotList(j, 1)+1)) == i
            currentSpotsX(counter) = spotList(j, 2);
            currentSpotsY(counter) = spotList(j, 1);
            currentSpotsRH(counter) = spotList(j, 3);    
            currentSpotsInt(counter) = spotList(j, 4);                
            counter = counter + 1;
            toKeep = [toKeep j];
        end        
    end
    
    currentSpotsRadius = currentSpotsRH; %currentSpotsRH(toKeep);
    if ~isempty(distMtx)
        CCDistMtx = distMtx(toKeep, toKeep);
    else
        CCDistMtx = [];
    end    
    [S, C] = graphconncomp(sparse(CCDistMtx), 'Weak', 'true');    
    
    %a very special case: if there is only one spot then the pairwise
    %distance function gives back an empty matrix, but still in that case
    %we wish to add that one point.
    if length(currentSpotsX) == 1
        S = 1;
        C = 1;
    end
    
    nbPoints = zeros(S, 1);
    
    for j=1:length(C)
       
        nbPoints(C(j)) = nbPoints(C(j)) + 1;
        
    end

    bigClusters = find(nbPoints > 0);
    
%    fprintf(fOut, 'fileName, Cell#, ClusterID, ClusterSize, DropletSizeMean, DropletSizeMedian, DropletSizeStd\n');    

%     for j=1:numel(bigClusters)
%         cid = bigClusters(j);
%         ids = find(C==cid);
%         radii = currentSpotsRadius(ids);
%         areas = radii.^2*pi;
%         fprintf(fOut, '%s, %d, %d, %d, %f, %f, %f\n', outName, i, j, nbPoints(bigClusters(j)), mean(areas), median(areas), std(areas));
%     end

    for j=1:numel(nbPoints)
        cid = j;
        ids = find(C==cid);
        radii = currentSpotsRadius(ids);        
        areas = radii.^2*pi;
        intensities = currentSpotsInt(ids);
        if nbPoints(j) > 1  
            if nargin<10
                fprintf(fOut, '%s, %d, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f, %f\n', outName, i, j, nbPoints(j), mean(areas), median(areas), std(areas), mean(areas) * scaleUm2, median(areas) * scaleUm2, std(areas) * scaleUm2,...
                mean(intensities), median(intensities), std(intensities));
                fprintf(fClusterBased, '%s, %d, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f, %f\n',...
                    outName, i, j, nbPoints(j), mean(areas), median(areas), std(areas), mean(areas) * scaleUm2, median(areas) * scaleUm2, std(areas) * scaleUm2,...
                    mean(intensities), median(intensities), std(intensities));                

            else
                fprintf(fOut, '%s, %d, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n', outName, i, j, nbPoints(j), mean(areas), median(areas), std(areas), mean(areas) * scaleUm2, median(areas) * scaleUm2, std(areas) * scaleUm2,...
                mean(intensities), median(intensities), std(intensities),...
                redIntensities(i,1),redIntensities(i,2));
                fprintf(fClusterBased, '%s, %d, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n',...
                outName, i, j, nbPoints(j), mean(areas), median(areas), std(areas), mean(areas) * scaleUm2, median(areas) * scaleUm2, std(areas) * scaleUm2,...
                    mean(intensities), median(intensities), std(intensities),...
                    redIntensities(i,1),redIntensities(i,2));
            end
        end
        for k=1:numel(radii)
            if nargin<10
                fprintf(fOutSpot, '%s, %d, %d, %d, %d, %f, %f\n', outName, i, j, nbPoints(j), uint16(areas(k)), areas(k) * scaleUm2, intensities(k));            
                fprintf(fSingleSpot, '%s, %d, %d, %d, %d, %f, %f\n', outName, i, j, nbPoints(j), uint16(areas(k)), areas(k) * scaleUm2, intensities(k));                        
            else
                fprintf(fOutSpot, '%s, %d, %d, %d, %d, %f, %f, %f, %f\n', outName, i, j, nbPoints(j), uint16(areas(k)), areas(k) * scaleUm2, intensities(k),...
                    redIntensities(i,1),redIntensities(i,2));            
                fprintf(fSingleSpot, '%s, %d, %d, %d, %d, %f, %f, %f, %f\n', outName, i, j, nbPoints(j), uint16(areas(k)), areas(k) * scaleUm2, intensities(k),...
                    redIntensities(i,1),redIntensities(i,2));                        
            end
        end                
    end
    
    currentSpotsArea = currentSpotsRadius.^2*pi;
    if numel(currentSpotsRadius) ~= 0
        if nargin<10
            fprintf(fOutCell, '%s, %d, %d, %f, %f, %f, %f, %f, %f\n', outName, i, numel(currentSpotsRadius), mean(currentSpotsArea), mean(currentSpotsArea) * scaleUm2, mean(currentSpotsInt), ...
                std(currentSpotsArea), std(currentSpotsArea) * scaleUm2, std(currentSpotsInt));
        else
            fprintf(fOutCell, '%s, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f\n', outName, i, numel(currentSpotsRadius), mean(currentSpotsArea), mean(currentSpotsArea) * scaleUm2, mean(currentSpotsInt), ...
                std(currentSpotsArea), std(currentSpotsArea) * scaleUm2, std(currentSpotsInt),...
                redIntensities(i,1),redIntensities(i,2));
        end
        if nargin<10
            fprintf(fCellBased, '%s, %d, %d, %f, %f, %f, %f, %f, %f\n', outName, i, numel(currentSpotsRadius), mean(currentSpotsArea), mean(currentSpotsArea) * scaleUm2, mean(currentSpotsInt), ...
                std(currentSpotsArea), std(currentSpotsArea) * scaleUm2, std(currentSpotsInt));
        else
            fprintf(fCellBased, '%s, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f\n', outName, i, numel(currentSpotsRadius), mean(currentSpotsArea), mean(currentSpotsArea) * scaleUm2, mean(currentSpotsInt), ...
                std(currentSpotsArea), std(currentSpotsArea) * scaleUm2, std(currentSpotsInt),...                
                redIntensities(i,1),redIntensities(i,2));
        end
    else
        if nargin<10
            fprintf(fOutCell, '%s, %d, %d, %f, %f, %f, %f, %f, %f\n', outName, i, numel(currentSpotsRadius), 0, 0, 0, 0, 0, 0);        
            fprintf(fCellBased, '%s, %d, %d, %f, %f, %f, %f, %f, %f\n',  outName, i, numel(currentSpotsRadius), 0, 0, 0, 0, 0, 0);
        else
            fprintf(fOutCell, '%s, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f\n', outName, i, numel(currentSpotsRadius), 0, 0, 0, 0, 0, 0, 0, 0);        
            fprintf(fCellBased, '%s, %d, %d, %f, %f, %f, %f, %f, %f, %f, %f\n',  outName, i, numel(currentSpotsRadius), 0, 0, 0, 0, 0, 0, 0, 0);
        end

    % put an empty fake line
        if nargin<10
            fprintf(fOutSpot, '%s, %d, %d, %d, %d, %d, %d\n', outName, i, 0, 0, 0, 0, 0);
            fprintf(fSingleSpot, '%s, %d, %d, %d, %d, %d, %d\n', outName, i, 0, 0, 0, 0, 0); 
        else
            fprintf(fOutSpot, '%s, %d, %d, %d, %d, %d, %d, %d, %d\n', outName, i, 0, 0, 0, 0, 0, 0, 0);            
            fprintf(fSingleSpot, '%s, %d, %d, %d, %d, %d, %d, %d, %d\n', outName, i, 0, 0, 0, 0, 0, 0, 0);                        
        end            
        
    end
end


fclose (fOut);

fclose (fOutSpot);

fclose(fOutCell);